include:
#  - template: Security/Container-Scanning.gitlab-ci.yml
  - project: "the-microservice-dungeon/devops-team/common-ci-cd"
    ref: "main"
    file: 
      - "helm/package-publish.yaml"

stages:
  - helm
  - build
  - check
  - containerize
  - container

services:
  - name: docker:dind
    command: ["--tls=false"]

variables:
  # This forces GitLab to only clone the latest commit of the current branch when running the pipeline.
  # This improves speed and reliability because it limits the amount of stuff that needs to be cloned on every run.
  GIT_DEPTH: 1
  # Disable the Gradle daemon for Continuous Integration servers as correctness
  # is usually a priority over speed in CI environments. Using a fresh
  # runtime for each build is more reliable since the runtime is completely
  # isolated from any previous builds.
  GRADLE_OPTS: "-Dorg.gradle.daemon=false"
  # Set Image reference path globally to avoid duplicates
  # ${CI_REGISTRY_IMAGE} is set to own repository
  #IMAGE_REF: ${CI_REGISTRY_IMAGE}
  IMAGE_REF: registry.gitlab.com/the-microservice-dungeon/devops-team/msd-image-registry/robot
  CHART_NAME: "robot"
  PATH_TO_CHART: "helm-chart"


  # not needed for kaniko kubernetes runner
  # DinD config
 # DOCKER_HOST: "tcp://docker:2375"
 # DOCKER_TLS_CERTDIR: ""
 # DOCKER_DRIVER: overlay2

before_script:
  - export GRADLE_USER_HOME="$(pwd)/.gradle"
  - chmod +x gradlew

after_script:
  - rm -f $GRADLE_USER_HOME/caches/modules-2/modules-2.lock
  - rm -fr $GRADLE_USER_HOME/caches/*/plugin-resolution/

cache: &gradle_cache
  key: "$CI_COMMIT_REF_SLUG"
  policy: pull-push
  paths:
    - "build"
    - "*/build"
    - ".gradle"

build:
  image: gradle:7-jdk17
  stage: build

  except:
    changes:
      - "helm-chart/**"

  script:
    - ./gradlew clean assemble --no-daemon --build-cache
  cache:
    <<: *gradle_cache
    policy: push

test:
  image: gradle:7-jdk17
  stage: check

  dependencies: [ build ]
  script:
    - ./gradlew clean test -PcreateReports --no-daemon --stacktrace --info --build-cache
    - cat build/jacocoHtml/index.html | grep -o '<tfoot>.*</tfoot>'
  cache:
    <<: *gradle_cache
    policy: pull
  artifacts:
    name: 'Test reports'
    when: always
    paths:
      - build/reports/
  coverage: '/Total.*?([0-9]{1,3})%/'

lint:
  image: gradle:7-jdk17

  stage: check

  script:
    - ./gradlew ktLintCheck --no-daemon
  allow_failure: true


# Analyze stage

containerize:
  only:
    - main
  image: gradle:7-jdk17

  stage: containerize
  dependencies: [ test ]

  script:
    - >
      args=( 
        "-Djib.to.image=${IMAGE_REF}"
        "-Djib.to.tags=${CI_COMMIT_BRANCH}"
        "-Djib.from.image=eclipse-temurin:17-jdk"
        "-Djib.from.platforms=linux/amd64,linux/arm64"
        "-Djib.to.auth.username=${CI_CONTAINER_USER}"
        "-Djib.to.auth.password=${CI_CONTAINER_PASSWORD}"
        "-Djib.container.creationTime=USE_CURRENT_TIMESTAMP"
      )
    - >
      if [ -n "${CI_COMMIT_TAG}" ];
      then
        args+=( "-Djib.to.tags=${CI_COMMIT_TAG},${CI_COMMIT_SHORT_SHA}" );
      elif [ "${CI_COMMIT_BRANCH}" == "${CI_DEFAULT_BRANCH}" ];
      then
        args+=( "-Djib.to.tags=latest,${CI_COMMIT_SHORT_SHA}" ); 
      fi
    - ./gradlew jib "${args[@]}" --stacktrace --info
  cache:
    <<: *gradle_cache
    policy: pull

#container_scanning:
#  stage: scan
#  variables:
#    CS_DEFAULT_BRANCH_IMAGE: $IMAGE_REF
#    CI_APPLICATION_TAG: $CI_COMMIT_TAG
#    CI_APPLICATION_REPOSITORY: $CI_REGISTRY_IMAGE
#  rules:
#    - if: $CI_COMMIT_TAG

helm-package-publish:
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      changes:
        - ${PATH_TO_CHART}/**/*
    - if: '$CI_PIPELINE_SOURCE == "web"'
      when: always
