package com.msd.robotservice.application.core

import com.msd.robotservice.domain.robot.exception.RobotNotFoundException
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.bind.annotation.*
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class RestAPIExceptionHandler : ResponseEntityExceptionHandler() {
  @ExceptionHandler(RobotNotFoundException::class)
  fun handleRobotNotFoundException(robotNotFoundException: RobotNotFoundException): ResponseEntity<Any> {
    logger.info("Request failed because no robot with the specified ID was found.")
    return ResponseEntity(robotNotFoundException.message, HttpStatus.NOT_FOUND)
  }

  override fun handleHttpMessageNotReadable(
    ex: HttpMessageNotReadableException,
    headers: HttpHeaders,
    status: HttpStatus,
    request: WebRequest
  ): ResponseEntity<Any> {
    logger.info("Unreadable HTTP Request: ${ex.message}")
    logger.info("More information: ${ex.mostSpecificCause}")
    logger.info("Headers: " + headers.toSortedMap())

    return ResponseEntity("Request could not be accepted", HttpStatus.BAD_REQUEST)
  }
}
