package com.msd.robotservice.application.command

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

data class RobotCommand(
  val robotId: UUID,
  val transactionId: UUID,
  val playerId: UUID,
  val command: CommandType,
  val planetId: UUID?,
  val targetId: UUID?
) {
  enum class CommandType {
    @JsonProperty("mining")
    MINING,
    @JsonProperty("movement")
    MOVEMENT,
    @JsonProperty("battle")
    BATTLE,
    @JsonProperty("regenerate")
    REGENERATE;
  }
}
