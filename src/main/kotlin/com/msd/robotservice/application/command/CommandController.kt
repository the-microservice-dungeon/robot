package com.msd.robotservice.application.command

import com.msd.robotservice.application.robot.handlers.MediatingCommandDispatcher
import mu.KotlinLogging
import org.springframework.core.env.Environment
import org.springframework.core.env.Profiles
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import kotlin.concurrent.thread

@RestController
@RequestMapping("/commands")
class CommandController(
  val mediatingCommandDispatcher: MediatingCommandDispatcher,
  val environment: Environment
) {
  val logger = KotlinLogging.logger { }

  /**
   * Receives a list of commands in a string representation and executes them asynchronously.
   *
   * @param commandDto A DTO containing the list of commands
   * @return ResponseEntity stating the success of the Command Queueing
   */
  @PostMapping(consumes = ["application/json"], produces = ["application/json"])
  fun receiveCommand(@RequestBody commandDto: CommandDto): ResponseEntity<Any> {
    if (commandDto.commands.isNotEmpty()) {
      logger.debug { "Received ${commandDto.commands.size} commands" }
      if (!environment.acceptsProfiles(Profiles.of("no-async"))) {
        thread(start = true, isDaemon = false) {
          mediatingCommandDispatcher.handleList(commandDto.commands)
        }
      } else
        mediatingCommandDispatcher.handleList(commandDto.commands)
    }
    return ResponseEntity.accepted().body("Command batch accepted")
  }
}
