package com.msd.robotservice.application.command

data class CommandDto(
  val commands: List<RobotCommand>
)
