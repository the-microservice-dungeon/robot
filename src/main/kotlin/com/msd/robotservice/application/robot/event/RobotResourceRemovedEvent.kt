package com.msd.robotservice.application.robot.event

import com.msd.robotservice.domain.planet.ResourceType
import com.msd.robotservice.domain.robot.Robot
import java.util.UUID

data class RobotResourceRemovedEvent(
  val robotId: UUID,
  val removedResource: ResourceType,
  val removedAmount: Int,
  val resourceInventory: Map<ResourceType, Int>
) : RobotEvent {

  companion object {
    fun build(robot: Robot, removedResource: ResourceType, removedAmount: Int) =
      RobotResourceRemovedEvent(
        robotId = robot.id,
        removedResource = removedResource,
        removedAmount = removedAmount,
        resourceInventory = robot.inventory.resources
      )
  }

  override fun key(): String = robotId.toString()
  override fun type(): String = "RobotResourceRemoved"
}
