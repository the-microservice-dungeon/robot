package com.msd.robotservice.application.robot.handlers

import com.msd.robotservice.application.command.RobotCommand
import com.msd.robotservice.application.robot.event.RobotAttackedEvent
import com.msd.robotservice.application.robot.event.RobotEventPublisher
import com.msd.robotservice.domain.robot.RobotRepository
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class FightingCommandHandler(
  val robotRepository: RobotRepository,
  val eventPublisher: RobotEventPublisher
) {

  @Transactional(rollbackOn = [Exception::class])
  fun handle(command: RobotCommand) {
    val targetId = command.targetId ?: throw RuntimeException("TargetId is null")

    val attacker = robotRepository.findByIdOrThrow(command.robotId)
    val target = robotRepository.findByIdOrThrow(targetId)

    attacker.attack(target)

    this.robotRepository.save(attacker)
    this.robotRepository.save(target)

    val event = RobotAttackedEvent.build(attacker, target)
    this.eventPublisher.publish(event, command.transactionId)
  }
}
