package com.msd.robotservice.application.robot.mappers

import com.msd.robotservice.application.robot.dtos.InventoryDto
import com.msd.robotservice.application.robot.dtos.RobotDto
import com.msd.robotservice.domain.planet.ResourceType
import com.msd.robotservice.domain.robot.Inventory
import com.msd.robotservice.domain.robot.Robot
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings

@Mapper
abstract class RobotMapper {

  @Mappings(
    Mapping(target = "planet", source = "planet.planetId"),
    Mapping(target = "inventory", source = "robot.inventory"),
    Mapping(target = "storageLevel", source = "robot.inventory.storageLevel"),
  )
  abstract fun robotToRobotDto(robot: Robot): RobotDto

  abstract fun robotsToRobotDtos(robots: List<Robot>): List<RobotDto>

  fun getInventory(inventory: Inventory): InventoryDto {
    return InventoryDto(
      inventory.maxStorage,
      inventory.usedStorage,
      inventory.getStorageUsageForResource(ResourceType.COAL),
      inventory.getStorageUsageForResource(ResourceType.IRON),
      inventory.getStorageUsageForResource(ResourceType.GEM),
      inventory.getStorageUsageForResource(ResourceType.GOLD),
      inventory.getStorageUsageForResource(ResourceType.PLATIN)
    )
  }
}
