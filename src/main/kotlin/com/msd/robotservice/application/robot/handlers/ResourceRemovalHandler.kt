package com.msd.robotservice.application.robot.handlers

import com.msd.robotservice.application.robot.event.RobotEventPublisher
import com.msd.robotservice.application.robot.event.RobotResourceRemovedEvent
import com.msd.robotservice.domain.planet.ResourceType
import com.msd.robotservice.domain.robot.RobotRepository
import mu.KotlinLogging
import org.springframework.stereotype.Service
import java.util.*

@Service
class ResourceRemovalHandler(
  val robotRepository: RobotRepository,
  val eventPublisher: RobotEventPublisher
) {
  companion object {
    val logger = KotlinLogging.logger {}
  }
  fun handle(robotId: UUID, resourceType: ResourceType, amount: Int, transactionId: UUID? = null) {
    val robot = robotRepository.findByIdOrThrow(robotId)
    robot.takeResources(resourceType, amount)
    this.robotRepository.save(robot)

    val event = RobotResourceRemovedEvent.build(robot, resourceType, amount)
    this.eventPublisher.publish(event, transactionId)
  }
}
