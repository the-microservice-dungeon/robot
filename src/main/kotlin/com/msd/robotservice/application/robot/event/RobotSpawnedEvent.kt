package com.msd.robotservice.application.robot.event

import com.msd.robotservice.domain.robot.Robot

data class RobotSpawnedEvent(
  val robot: Robot,
) : RobotEvent {

  companion object {
    fun build(robot: Robot) =
      RobotSpawnedEvent(
        robot = robot
      )
  }

  override fun key(): String = robot.id.toString()
  override fun type(): String = "RobotSpawned"
}
