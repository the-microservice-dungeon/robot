package com.msd.robotservice.application.robot.event

import java.util.UUID

interface RobotEventPublisher {
    fun publish(
      event: RobotEvent,
      transactionId: UUID? = null,
    )
}
