package com.msd.robotservice.application.robot.exception

import com.msd.robotservice.application.core.FailureException

/**
 * Throw this Exception if a player requests a MovementCommand that can not be executed, because the target planet is
 * too far away.
 */
class TargetPlanetNotReachableException(s: String) : FailureException(s)
