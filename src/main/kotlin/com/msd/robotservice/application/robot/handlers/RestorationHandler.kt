package com.msd.robotservice.application.robot.handlers

import com.msd.robotservice.application.robot.RestorationType
import com.msd.robotservice.application.robot.event.RobotEventPublisher
import com.msd.robotservice.application.robot.event.RobotRestoredAttributesEvent
import com.msd.robotservice.domain.robot.RobotRepository
import mu.KotlinLogging
import org.springframework.stereotype.Service
import java.util.*

@Service
class RestorationHandler(
  val robotRepository: RobotRepository,
  val eventPublisher: RobotEventPublisher
) {
  companion object {
    val logger = KotlinLogging.logger {}
  }

  fun handle(robotId: UUID, type: RestorationType, transactionId: UUID? = null) {
    val robot = robotRepository.findByIdOrThrow(robotId)
    when (type) {
      RestorationType.ENERGY -> robot.restoreEnergy()
      RestorationType.HEALTH -> robot.repair()
    }
    this.robotRepository.save(robot)

    val event = RobotRestoredAttributesEvent.build(robot, type)
    this.eventPublisher.publish(event, transactionId)
  }
}
