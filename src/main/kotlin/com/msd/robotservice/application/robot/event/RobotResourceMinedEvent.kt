package com.msd.robotservice.application.robot.event

import com.msd.robotservice.domain.planet.ResourceType
import com.msd.robotservice.domain.robot.Robot
import java.util.UUID

data class RobotResourceMinedEvent(
  val robotId: UUID,
  val minedResource: ResourceType,
  val minedAmount: Int,
  val resourceInventory: Map<ResourceType, Int>
) : RobotEvent {

  companion object {
    fun build(robot: Robot, minedResource: ResourceType, minedAmount: Int) =
      RobotResourceMinedEvent(
        robotId = robot.id,
        minedResource = minedResource,
        minedAmount = minedAmount,
        resourceInventory = robot.inventory.resources
      )
  }

  override fun key(): String = robotId.toString()
  override fun type(): String = "RobotResourceMined"
}
