package com.msd.robotservice.application.robot.event

import com.msd.robotservice.domain.robot.Robot
import com.msd.robotservice.domain.robot.UpgradeType
import java.util.UUID

data class RobotUpgradedEvent(
  val robotId: UUID,
  val upgrade: UpgradeType,
  val level: Int,
  val robot: Robot
) : RobotEvent {
  companion object {
    fun build(robot: Robot, type: UpgradeType, level: Int) =
      RobotUpgradedEvent(
        robotId = robot.id,
        upgrade = type,
        level = level,
        robot = robot
      )
  }

  override fun key(): String = robotId.toString()
  override fun type(): String = "RobotUpgraded"
}
