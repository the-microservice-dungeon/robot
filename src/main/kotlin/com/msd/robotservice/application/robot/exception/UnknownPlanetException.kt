package com.msd.robotservice.application.robot.exception

import com.msd.robotservice.application.core.FailureException
import java.util.*

class UnknownPlanetException(planetId: UUID) :
  FailureException("Planet with ID $planetId not found")
