package com.msd.robotservice.application.robot.handlers

import com.msd.robotservice.application.command.RobotCommand
import com.msd.robotservice.application.robot.event.RobotEventPublisher
import com.msd.robotservice.application.robot.event.RobotRegeneratedEvent
import com.msd.robotservice.domain.robot.RobotRepository
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class RegenerateCommandHandler(
  val robotRepository: RobotRepository,
  val eventPublisher: RobotEventPublisher
) {

  @Transactional(rollbackOn = [Exception::class])
  fun handle(command: RobotCommand) {
    val robot = robotRepository.findByIdOrThrow(command.robotId)
    robot.regenerate()
    this.robotRepository.save(robot)

    val event = RobotRegeneratedEvent.build(robot)
    this.eventPublisher.publish(event, command.transactionId)
  }
}
