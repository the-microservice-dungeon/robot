package com.msd.robotservice.application.robot.event

import com.msd.robotservice.domain.planet.Planet
import com.msd.robotservice.domain.robot.Robot
import java.util.UUID

data class RobotMovedEvent(
  val robotId: UUID,
  val fromPlanet: PlanetMovement,
  val toPlanet: PlanetMovement,
  val remainingEnergy: Int
) : RobotEvent {

  data class PlanetMovement(
    val id: UUID,
    val movementDifficulty: Int
  )
  companion object {
    fun build(robot: Robot, from: Planet, to: Planet) =
      RobotMovedEvent(
        robotId = robot.id,
        fromPlanet = PlanetMovement(from.planetId, from.movementDifficulty),
        toPlanet = PlanetMovement(to.planetId, to.movementDifficulty),
        remainingEnergy = robot.energy
      )
  }

  override fun key(): String = robotId.toString()

  override fun type(): String = "RobotMoved"
}
