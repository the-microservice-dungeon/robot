package com.msd.robotservice.application.robot.event

sealed interface RobotEvent {
  fun key(): String
  fun type(): String
}
