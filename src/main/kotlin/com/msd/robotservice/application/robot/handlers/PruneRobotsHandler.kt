package com.msd.robotservice.application.robot.handlers

import com.msd.robotservice.domain.robot.RobotRepository
import mu.KotlinLogging
import org.springframework.stereotype.Service

@Service
class PruneRobotsHandler(
  val robotRepository: RobotRepository
) {
  companion object {
    val logger = KotlinLogging.logger {}
  }

  fun handle() {
    robotRepository.deleteAll()
  }
}
