package com.msd.robotservice.application.robot.dtos

import com.msd.robotservice.domain.robot.UpgradeType
import java.util.*

data class UpgradeDto(
  val transactionId: UUID,

  val upgradeType: UpgradeType,

  val targetLevel: Int
)
