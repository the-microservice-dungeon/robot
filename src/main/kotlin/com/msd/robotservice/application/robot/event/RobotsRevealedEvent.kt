package com.msd.robotservice.application.robot.event

import com.msd.robotservice.domain.robot.Robot
import java.util.*

data class RobotsRevealedEvent(
  val robots: List<RobotRevealedDto>
) : RobotEvent {
  companion object {
    data class RobotRevealedDto(
      val robotId: UUID,
      val planetId: UUID,
      val playerNotion: String,
      val health: Int,
      val energy: Int,
      val levels: RobotLevels,
    )
    data class RobotLevels(
      var healthLevel: Int,
      var damageLevel: Int,
      var miningSpeedLevel: Int,
      val miningLevel: Int,
      val energyLevel: Int,
      val energyRegenLevel: Int,
      val storageLevel: Int
    )

    fun build(robots: List<Robot>): RobotsRevealedEvent {
      return RobotsRevealedEvent(
        robots.map { robot ->
          RobotRevealedDto(
            robot.id,
            robot.planet.planetId,
            robot.player.toString().substringBefore('-'),
            robot.health,
            robot.energy,
            RobotLevels(
              robot.healthLevel,
              robot.damageLevel,
              robot.miningSpeedLevel,
              robot.miningLevel,
              robot.energyLevel,
              robot.energyRegenLevel,
              robot.inventory.storageLevel
            )
          )
        }
      )
    }
  }

  override fun key(): String = ""
  override fun type(): String = "RobotsRevealed"
}
