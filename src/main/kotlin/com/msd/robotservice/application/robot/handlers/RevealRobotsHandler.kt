package com.msd.robotservice.application.robot.handlers

import com.msd.robotservice.application.robot.event.RobotEventPublisher
import com.msd.robotservice.application.robot.event.RobotsRevealedEvent
import com.msd.robotservice.domain.robot.RobotRepository
import mu.KotlinLogging
import org.springframework.stereotype.Service

@Service
class RevealRobotsHandler(
  val robotRepository: RobotRepository,
  val eventPublisher: RobotEventPublisher
) {
  companion object {
    val logger = KotlinLogging.logger {}
  }

  fun handle() {
    val robots = robotRepository.findAllAlive()

    val event = RobotsRevealedEvent.build(robots)
    this.eventPublisher.publish(event)
  }
}
