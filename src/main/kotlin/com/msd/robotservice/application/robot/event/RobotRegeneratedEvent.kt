package com.msd.robotservice.application.robot.event

import com.msd.robotservice.domain.robot.Robot
import java.util.UUID

data class RobotRegeneratedEvent(
  val robotId: UUID,
  val availableEnergy: Int
) : RobotEvent {

  companion object {
    fun build(robot: Robot) =
      RobotRegeneratedEvent(
        robotId = robot.id,
        availableEnergy = robot.energy
      )
  }

  override fun key(): String = robotId.toString()
  override fun type(): String = "RobotRegenerated"
}
