package com.msd.robotservice.application.event

import java.util.*

abstract class ErrorEvent(
  open val playerId: UUID,
  open val transactionId: UUID,
  open val robotId: UUID,
  val code: String,
  open val description: String,
  open val details: String = ""
)
