/**
 * Introduces a debug mode into the application that can simulate a game. It is just used for
 * development and debugging purposes. Activate the `debug` profile to activate the debug mode
 */
package com.msd.robotservice.application.debug.mode;
