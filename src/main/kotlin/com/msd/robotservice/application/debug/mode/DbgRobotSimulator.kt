package com.msd.robotservice.application.debug.mode

import com.msd.robotservice.application.command.RobotCommand
import com.msd.robotservice.application.robot.handlers.MediatingCommandDispatcher
import com.msd.robotservice.application.robot.handlers.SpawnRobotHandler
import com.msd.robotservice.domain.planet.Planet
import com.msd.robotservice.domain.planet.PlanetRepository
import com.msd.robotservice.domain.planet.ResourceType
import com.msd.robotservice.domain.robot.Robot
import com.msd.robotservice.domain.robot.RobotRepository
import mu.KotlinLogging
import org.springframework.boot.context.event.ApplicationStartedEvent
import org.springframework.context.ApplicationListener
import org.springframework.context.annotation.Profile
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.util.*

/**
 * Simulates the lifecycle of robots after startup when the debug switch is enabled.
 * Reason for implementation: Allows demonstration and validation while not relying on a
 * player implementation and running game to trigger a workflow.
 */
@Service
@Profile("debug")
class DbgRobotSimulator(
  val spawnRobotHandler: SpawnRobotHandler,
  val planetRepository: PlanetRepository,
  val robotRepository: RobotRepository,
  val mediatingCommandDispatcher: MediatingCommandDispatcher
) : ApplicationListener<ApplicationStartedEvent> {
  val logger = KotlinLogging.logger { }
  val robots = mutableListOf<Robot>()
  val random = Random()
  val gameworldId: UUID = UUID.randomUUID()
  val playerId = UUID.randomUUID()
  val planets = listOf(
    Planet.of(
      UUID.fromString("00000000-0000-0000-0000-000000000000"),
      gameworldId,
      1,
    ),
    Planet.of(
      UUID.fromString("00000000-0000-0000-0000-000000000001"),
      gameworldId,
      2,
      ResourceType.COAL
    ),
    Planet.of(
      UUID.fromString("00000000-0000-0000-0000-000000000002"),
      gameworldId,
      3,
      ResourceType.GEM,
    ),
    Planet.of(
      UUID.fromString("00000000-0000-0000-0000-000000000003"),
      gameworldId,
      2,
      ResourceType.IRON,
    ),
    Planet.of(
      UUID.fromString("00000000-0000-0000-0000-000000000004"),
      gameworldId,
      1,
      ResourceType.PLATIN,
    ),
    Planet.of(
      UUID.fromString("00000000-0000-0000-0000-000000000005"),
      gameworldId,
      1,
      null,
    ),
    Planet.of(
      UUID.fromString("00000000-0000-0000-0000-000000000006"),
      gameworldId,
      3,
      ResourceType.GOLD,
    ),
    Planet.of(
      UUID.fromString("00000000-0000-0000-0000-000000000007"),
      gameworldId,
      1,
      null,
    )
  )

  override fun onApplicationEvent(event: ApplicationStartedEvent) {
    logger.info { "[DEBUG] Debug mode enabled" }
    logger.info { "[DEBUG] Initializing..." }

    planetRepository.saveAll(planets)
    val planetsWithNeighbours = planets.map { p1 ->
      planets.forEach { p2 -> p1.addNeighbour(p2) }
      p1
    }
    planetRepository.saveAll(planetsWithNeighbours)

    logger.info { "[DEBUG] Starting robot simulation..." }
    spawnRobotHandler.handle(playerId)
    robots.clear()
    robots.addAll(robotRepository.findAllByPlayer(playerId))
  }

  @Scheduled(fixedRate = 500)
  fun simulate() {
    logger.info { "[DEBUG] Random Robot action..." }
    val randomPlanet = planets[random.nextInt(0, planets.size)]

    if (robots.size < 1) {
      spawnRobotHandler.handle(playerId)
      robots.clear()
      robots.addAll(robotRepository.findAllByPlayer(playerId))
      return
    }

    val randomRobot = robots[random.nextInt(0, robots.size)]
    val targetRobot = robots[random.nextInt(0, robots.size)]
    val randomAction = random.nextInt(0, 7)
    val commands: MutableList<RobotCommand> = mutableListOf()
    when (randomAction) {
      0 -> {
        // Ensure that we don't kill our entire fleet
        if (robots.size > 1) {
          commands.add(RobotCommand(randomRobot.id, UUID.randomUUID(), randomRobot.player, RobotCommand.CommandType.BATTLE, null, targetRobot.id))
        }
      }

      1 -> {
        commands.add(RobotCommand(randomRobot.id, UUID.randomUUID(), randomRobot.player, RobotCommand.CommandType.MOVEMENT, randomPlanet.planetId, null))
      }
      2 -> {
        commands.add(RobotCommand(randomRobot.id, UUID.randomUUID(), randomRobot.player, RobotCommand.CommandType.MINING, null, null))
      }
      3 -> {
        commands.add(RobotCommand(randomRobot.id, UUID.randomUUID(), randomRobot.player, RobotCommand.CommandType.REGENERATE, null, null))
      }
      4 -> {
        logger.info { "[DEBUG] Spawning a new robot and adding to fleet" }
        spawnRobotHandler.handle(UUID.randomUUID())
        robots.clear()
        robots.addAll(robotRepository.findAllByPlayer(playerId))
      }

      else -> logger.warn { "[DEBUG] No Action registered for $randomAction" }
    }

    logger.info {
      "[DEBUG] Executing ${commands.size} commands of types '${
      commands.map { it::class.simpleName }.joinToString()
      }'..."
    }

    mediatingCommandDispatcher.handleList(commands)
  }
}
