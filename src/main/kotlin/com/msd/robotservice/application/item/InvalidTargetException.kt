package com.msd.robotservice.application.item

class InvalidTargetException(s: String) : RuntimeException(s)
