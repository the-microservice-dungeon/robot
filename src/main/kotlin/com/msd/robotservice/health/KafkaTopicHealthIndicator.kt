package com.msd.robotservice.health

import com.msd.robotservice.application.config.properties.TopicConfigurationProperties
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.actuate.health.Health
import org.springframework.boot.actuate.health.HealthIndicator
import org.springframework.boot.actuate.health.Status
import org.springframework.kafka.core.KafkaAdminOperations
import org.springframework.stereotype.Component

/**
 * A custom health monitor checking the availability of producer topics.
 *
 * @author Daniel Köllgen
 * @since 09.11.2024
 */
@Component
class KafkaTopicHealthIndicator @Autowired constructor(

    private val admin: KafkaAdminOperations,
    private val topicConfig: TopicConfigurationProperties

) : HealthIndicator {

    /**
     * Verifies, that each configured topic has been created with an aggregated health status.
     */
    override fun health(): Health {
        val topics: List<String> = topicConfig.config.keys.toList()

        val details: Map<String, Status> = topics.fold(mutableMapOf()) { acc, element ->
            try {
                admin.describeTopics(element)
                acc[element] = Status.UP
            } catch (e: Exception) {
                acc[element] = Status.DOWN
            }
            acc
        }
        val status: Status = details.values.fold(Status.UP) { acc: Status, element: Status ->
            if (acc != Status.UP) {
                acc
            } else {
                element
            }
        }
        return Health.status(status).withDetails(details).build()
    }
}
