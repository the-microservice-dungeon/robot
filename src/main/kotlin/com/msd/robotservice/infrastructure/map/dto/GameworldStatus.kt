package com.msd.robotservice.infrastructure.map.dto

import java.util.UUID

enum class GameworldStatus {
  ACTIVE, INACTIVE
}
