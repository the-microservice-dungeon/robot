package com.msd.robotservice.infrastructure.map.dto

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.UUID

data class GameworldCreatedEventDto(
  val id: UUID,
  val status: String,
  val planets: List<GameworldCreatedEventPlanetDto>,
) {
  data class GameworldCreatedEventPlanetDto(
    val id: UUID,
    val x: Int,
    val y: Int,
    val movementDifficulty: Int,
    val resource: PlanetResourceDto?
  )
}
