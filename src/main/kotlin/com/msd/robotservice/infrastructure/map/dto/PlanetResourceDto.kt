package com.msd.robotservice.infrastructure.map.dto

import com.fasterxml.jackson.annotation.JsonProperty
import com.msd.robotservice.domain.planet.ResourceType

data class PlanetResourceDto(
  val resourceType: ResourceType,
  val maxAmount: Int,
  val currentAmount: Int
)
