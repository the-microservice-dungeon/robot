package com.msd.robotservice.infrastructure.map

/**
 * Gets thrown if there is a problem with the connection to another MicroService or if an internal problem occurs
 * in another MicroService during processing of one of our requests.
 */
class ClientException : RuntimeException {
  constructor(s: String) : super(s)
  constructor(s: String, cause: Throwable) : super(s, cause)
}
