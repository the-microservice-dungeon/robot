package com.msd.robotservice.infrastructure.map

import reactor.core.publisher.Mono
import java.util.*

interface GameMapService {
  fun mine(planetId: UUID, amount: Int): Mono<Int>
}
