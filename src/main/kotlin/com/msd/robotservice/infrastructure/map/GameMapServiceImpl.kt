package com.msd.robotservice.infrastructure.map

import com.msd.robotservice.application.config.properties.MicroserviceMapConfigurationProperties
import com.msd.robotservice.application.robot.exception.UnknownPlanetException
import com.msd.robotservice.infrastructure.map.dto.MineRequestDto
import com.msd.robotservice.infrastructure.map.dto.MineResponseDto
import io.netty.channel.ChannelOption
import io.netty.handler.timeout.ReadTimeoutHandler
import io.netty.handler.timeout.WriteTimeoutHandler
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.client.reactive.ReactorClientHttpConnector
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.WebClientRequestException
import reactor.core.publisher.Mono
import reactor.netty.http.client.HttpClient
import java.time.Duration
import java.util.*
import java.util.concurrent.TimeUnit

@Service
class GameMapServiceImpl(
  @Autowired val mapConfig: MicroserviceMapConfigurationProperties
) : GameMapService {
  private val gameMapClient: WebClient
  private val logger = KotlinLogging.logger { }

  companion object {
    const val PLANETS_URI = "/planets"
  }

  init {
    val httpClient: HttpClient = HttpClient.create()
      .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 5000)
      .responseTimeout(Duration.ofMillis(5000))
      .doOnConnected { conn ->
        conn.addHandlerLast(ReadTimeoutHandler(5000, TimeUnit.MILLISECONDS))
          .addHandlerLast(WriteTimeoutHandler(5000, TimeUnit.MILLISECONDS))
      }

    val mapUrl = if (this.mapConfig.address.startsWith("http://")) this.mapConfig.address else "http://${this.mapConfig.address}"

    logger.debug { "Using $mapUrl to send commands to map" }

    gameMapClient = WebClient.builder()
      .baseUrl(mapUrl)
      .defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
      .clientConnector(ReactorClientHttpConnector(httpClient))
      .build()
  }

  override fun mine(planetId: UUID, amount: Int): Mono<Int> = gameMapClient.post()
    .uri("$PLANETS_URI/$planetId/minings")
    .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
    .body(Mono.just(MineRequestDto(amount)), MineRequestDto::class.java)
    .exchangeToMono { response ->
      when (response.statusCode()) {
        HttpStatus.OK -> response.bodyToMono(MineResponseDto::class.java)
        HttpStatus.NOT_FOUND -> throw UnknownPlanetException(planetId)
        else -> throw ClientException("Map Service returned internal error when trying to mine on planet $planetId")
      }
    }
    .map { it.amountMined }
    .onErrorMap(WebClientRequestException::class.java) { err ->
      throw ClientException(
        "Map Service not reachable",
        err
      )
    }
}
