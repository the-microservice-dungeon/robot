package com.msd.robotservice.infrastructure.map

import com.msd.robotservice.application.core.FailureException
import java.util.*

class NoResourceOnPlanetException(planetId: UUID) :
  FailureException("Map Service did not return any resource on the planet $planetId")
