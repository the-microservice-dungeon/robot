package com.msd.robotservice.infrastructure.game.dto

import com.fasterxml.jackson.annotation.JsonAlias

enum class GameStatus {
  @JsonAlias("created")
  CREATED,
  @JsonAlias("started")
  STARTED,
  @JsonAlias("ended")
  ENDED
}
