package com.msd.robotservice.infrastructure.game.dto

import java.util.UUID

data class GameStatusEvent (
    val gameId: UUID,
    val status: GameStatus
) {
}
