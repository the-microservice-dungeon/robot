package com.msd.robotservice.infrastructure.game.dto

import java.util.UUID

data class RoundStatusEvent (
    val gameId: UUID,
    val roundId: UUID,
    val roundNumber: Int,
    val roundStatus: RoundStatus
) {
}
