package com.msd.robotservice.domain.planet

enum class PlanetType(val stringValue: String) {
  DEFAULT("default"),
  SPACE_STATION("spacestation")
}
