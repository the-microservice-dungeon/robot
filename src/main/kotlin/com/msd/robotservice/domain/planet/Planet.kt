package com.msd.robotservice.domain.planet

import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.Type
import java.util.*
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.Id
import javax.persistence.ManyToMany

@Entity
data class Planet private constructor(
  @Id
  @Type(type = "uuid-char")
  val planetId: UUID,
  @Type(type = "uuid-char")
  val gameWorldId: UUID,
  var movementDifficulty: Int = 1,
  var resourceType: ResourceType? = null,
  @JsonIgnore
  @ManyToMany(fetch = FetchType.EAGER)
  var neighbours: MutableList<Planet> = mutableListOf()
) {
  companion object {
    /**
     * Fabric method to create a new planet.
     * Adds discovering event.
     */
    fun of(
      planetId: UUID,
      gameWorldId: UUID,
      movementDifficulty: Int = 1,
      resourceType: ResourceType? = null,
      neighbours: MutableList<Planet> = mutableListOf()
    ): Planet {
      return Planet(planetId, gameWorldId, movementDifficulty, resourceType, neighbours)
    }
  }

  fun addNeighbour(planet: Planet) {
    this.neighbours.add(planet)
  }

  fun removeNeighbour(planet: Planet) {
    this.neighbours.removeIf { it == planet || it.planetId == planet.planetId }
  }

  fun isNeighbourTo(planetId: UUID) = this.neighbours.any { it.planetId == planetId }
  override fun toString(): String {
    return "Planet(planetId=$planetId, gameWorldId=$gameWorldId, movementDifficulty=$movementDifficulty, resourceType=$resourceType, neighbours=${neighbours.map { it.planetId }})"
  }
}
