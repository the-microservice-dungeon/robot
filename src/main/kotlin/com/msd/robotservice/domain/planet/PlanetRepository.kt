package com.msd.robotservice.domain.planet

import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import java.util.*

interface PlanetRepository : CrudRepository<Planet, UUID> {
  @Query("select p.neighbours from Planet p where p.planetId = ?1")
  fun findNeighboursOfPlanet(planetId: UUID): List<Planet>

  @Query("select p from Planet p where p.gameWorldId = ?1")
  fun findPlanetsInGameworld(gameworldId: UUID): List<Planet>

  fun getRandomPlanetInGameworld(gameworldId: UUID): Planet = findPlanetsInGameworld(gameworldId).shuffled().first()
  fun getRandomPlanet(): Planet = findAll().shuffled().first()

  @Modifying
  @Query("delete from Planet p where p.gameWorldId = ?1")
  fun deleteAllPlanetsInGameworld(gameworldId: UUID)
}
