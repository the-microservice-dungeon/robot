package com.msd.robotservice.domain.robot

import com.fasterxml.jackson.annotation.JsonIgnore
import com.msd.robotservice.domain.planet.ResourceType
import com.msd.robotservice.domain.robot.exception.InventoryFullException
import com.msd.robotservice.domain.robot.exception.UpgradeException
import org.hibernate.annotations.Type
import java.util.*
import javax.persistence.ElementCollection
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.Id

@Entity
class Inventory {
  @Id
  @Type(type = "uuid-char")
  @JsonIgnore
  val id: UUID = UUID.randomUUID()
  var storageLevel: Int = 0
    internal set(value) {
      if (value > 5) throw UpgradeException("Max Storage Level has been reached. Upgrade not possible.")
      else if (value > storageLevel + 1)
        throw UpgradeException(
          "Cannot skip upgrade levels. Tried to upgrade from level $storageLevel to level $value"
        )
      else if (value <= storageLevel)
        throw UpgradeException("Cannot downgrade Robot. Tried to go from level $storageLevel to level $value")
      field = value
    }
  val maxStorage
    get() = UpgradeValues.storageByLevel[storageLevel]
  var usedStorage = 0
    protected set

  @ElementCollection(fetch = FetchType.EAGER)
  val resources = mutableMapOf(
    ResourceType.COAL to 0,
    ResourceType.IRON to 0,
    ResourceType.GEM to 0,
    ResourceType.GOLD to 0,
    ResourceType.PLATIN to 0,
  )

  /**
   * Adds a resource to this inventory. The inventory can hold all resources simultaneously, but the amount of
   * resources held cannot exceed <code>maxStorage</code>.
   *
   * @param resource  the resource which will be added to the inventory
   * @param amount    the amount that will be added
   */
  internal fun addResource(resource: ResourceType, amount: Int) {
    if(usedStorage >= maxStorage) throw InventoryFullException("Inventory is full. Cannot add more resources.")

    val newUsedStorage = usedStorage + amount

    if (newUsedStorage > maxStorage) {
      resources[resource] = resources[resource]!! + amount - (newUsedStorage - maxStorage)
      usedStorage = maxStorage
    } else {
      resources[resource] = resources[resource]!! + amount
      usedStorage += amount
    }
  }

  internal fun takeResource(resource: ResourceType, amount: Int) {
    val amountAvailable = resources[resource]!!
    val amountToTake = Math.min(amountAvailable, amount)
    resources[resource] = resources[resource]!! - amountToTake
    usedStorage -= amountToTake
  }

  fun getResourceAmount(resource: ResourceType): Int {
    return resources[resource]!!
  }

  /**
   * Returns the stored amount of a given resource. The resource will still remain in the inventory
   *
   * @param resource  the resource of which the amount should be returned
   * @return the stored amount of the resource as an <code>Int</code>
   */
  fun getStorageUsageForResource(resource: ResourceType): Int {
    return resources[resource]!!
  }

  /**
   * Removes all resources from the inventory.
   *
   * @return a list of all the resources taken
   */
  internal fun takeAllResources(): MutableMap<ResourceType, Int> {
    val takenResources = mutableMapOf<ResourceType, Int>()
    ResourceType.values().forEach {
      val amount = resources[it]!!
      takenResources[it] = amount
      usedStorage -= amount
      resources[it] = 0
    }
    return takenResources
  }

  /**
   * Checks if the `Inventory` of this `Robot` is full.
   *
   * @return `true` if the inventory is full, otherwise `false`
   */
  fun isFull(): Boolean {
    return maxStorage - usedStorage == 0
  }
}
