package com.msd.robotservice.domain.robot.exception

class NotEnoughResourcesException(s: String) : RuntimeException(s)
