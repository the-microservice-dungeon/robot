package com.msd.robotservice.domain.robot.exception

import com.msd.robotservice.application.core.FailureException

class TargetRobotOutOfReachException(s: String) : FailureException(s)
