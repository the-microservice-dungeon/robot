package com.msd.robotservice.domain.robot.exception

class InventoryFullException(s: String) : RuntimeException(s)
