package com.msd.robotservice.domain.robot

import com.msd.robotservice.domain.planet.Planet
import com.msd.robotservice.domain.planet.ResourceType
import com.msd.robotservice.domain.robot.exception.InventoryFullException
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import org.junit.jupiter.api.assertThrows
import java.util.*

class InventoryTest {

  private lateinit var robot1: Robot

  @BeforeEach
  fun initialize() {
    robot1 = Robot.of(UUID.randomUUID(), Planet.of(UUID.randomUUID(), UUID.randomUUID()))
  }

  @Test
  fun `Should throw when inventory is full`() {
    // then
    robot1.inventory.addResource(ResourceType.COAL, 20)
    assertThrows<InventoryFullException> {
      robot1.inventory.addResource(ResourceType.COAL, 25)
    }
  }

  @Test
  fun `Used storage can't exceed max storage`() {
    // then
    robot1.inventory.addResource(ResourceType.COAL, 25)

    assertEquals(20, robot1.inventory.usedStorage)
    assertEquals(20, robot1.inventory.getStorageUsageForResource(ResourceType.COAL))
  }

  @Test
  fun `Used storage is correct when holding different resources`() {
    // when
    robot1.inventory.addResource(ResourceType.COAL, 5)
    robot1.inventory.addResource(ResourceType.IRON, 10)
    robot1.inventory.addResource(ResourceType.PLATIN, 5)
    // then
    assertEquals(20, robot1.inventory.usedStorage)
  }

  @Test
  fun `Inventory can hold multiple resource`() {
    // when
    robot1.inventory.addResource(ResourceType.COAL, 5)
    robot1.inventory.addResource(ResourceType.IRON, 10)
    robot1.inventory.addResource(ResourceType.PLATIN, 5)
    // then
    assertAll(
      {
        assertEquals(5, robot1.inventory.getStorageUsageForResource(ResourceType.COAL))
      },
      {
        assertEquals(10, robot1.inventory.getStorageUsageForResource(ResourceType.IRON))
      },
      {
        assertEquals(5, robot1.inventory.getStorageUsageForResource(ResourceType.PLATIN))
      }
    )
  }

  @Test
  fun `Upgrading storage allows robot to hold more resources`() {
    // given
    robot1.upgrade(UpgradeType.STORAGE, 1)
    // when
    robot1.inventory.addResource(ResourceType.IRON, 25)
    // then
    assertEquals(50, robot1.inventory.maxStorage)
    assertEquals(25, robot1.inventory.getStorageUsageForResource(ResourceType.IRON))
  }

  @Test
  fun `correctly detemines if inventory is full or not`() {
    // given
    assertEquals(false, robot1.inventory.isFull())
    // when
    robot1.inventory.addResource(ResourceType.COAL, 20)
    // then
    assert(robot1.inventory.isFull())
  }
}
