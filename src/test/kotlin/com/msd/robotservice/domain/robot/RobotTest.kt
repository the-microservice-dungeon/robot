package com.msd.robotservice.domain.robot

import com.msd.robotservice.domain.planet.Planet
import com.msd.robotservice.domain.robot.exception.TargetRobotOutOfReachException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.util.*

internal class RobotTest {

  private val robot = Robot.of(UUID.randomUUID(), Planet.of(UUID.randomUUID(), UUID.randomUUID()))

  @Test
  fun `when move from planet Should reduce energy`() {
    val toPlanet = Planet.of(UUID.randomUUID(), UUID.randomUUID(), 2)

    robot.move(toPlanet)

    assertThat(robot.energy).isEqualTo(18)
  }

  @Test
  fun `when receive damage should reduce health`() {
    robot.receiveDamage(5)

    assertThat(robot.health).isEqualTo(5)
  }

  @Test
  fun `when receive damage below health should kill robot`() {
    robot.receiveDamage(robot.health + 10)

    assertThat(robot.alive).isFalse
  }

  @Test
  fun `when kill should kill robot`() {
    robot.kill()

    assertThat(robot.alive).isFalse
  }

  @Test
  fun `when attack robot not on planet Should throw `() {
    val target = Robot.of(UUID.randomUUID(), Planet.of(UUID.randomUUID(), UUID.randomUUID()))
    assertThrows<TargetRobotOutOfReachException> { robot.attack(target) }
  }

  @Test
  fun `when attack dead robot Should throw `() {
    val target = Robot.of(UUID.randomUUID(), Planet.of(UUID.randomUUID(), UUID.randomUUID()))
    target.kill()
    assertThrows<Exception> { robot.attack(target) }
  }


  @Test
  fun `when attack robot Should reduce energy`() {
    val target = Robot.of(UUID.randomUUID(), robot.planet)

    robot.attack(target)

    assertThat(robot.energy).isEqualTo(19)
  }

  @Test
  fun `when attack robot Should receive damage`() {
    val target = Robot.of(UUID.randomUUID(), robot.planet)

    robot.attack(target)

    assertThat(target.health).isEqualTo(9)
  }
}
