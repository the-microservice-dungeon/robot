package com.msd.robotservice.domain.robot

import com.msd.robotservice.AbstractIntegrationTest
import com.msd.robotservice.domain.planet.Planet
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Transactional
class RobotRepositoryIntegrationTest(
  @Autowired val robotRepository: RobotRepository
) : AbstractIntegrationTest() {

  // TODO fix these tests not running when they're not the first. Some tests drops the database and these tests don't pass anymore since there is no database
  val player1Id: UUID = UUID.randomUUID()
  val player2Id: UUID = UUID.randomUUID()

  lateinit var planet1: Planet
  lateinit var planet2: Planet

  lateinit var robot1: Robot
  lateinit var robot2: Robot
  lateinit var robot3: Robot
  lateinit var robot4: Robot
  lateinit var robot5: Robot
  lateinit var robot6: Robot

  @BeforeEach
  fun setup() {
    planet1 = Planet.of(UUID.randomUUID(), UUID.randomUUID())
    planet2 = Planet.of(UUID.randomUUID(), UUID.randomUUID())

    robot1 = Robot.of(player1Id, planet1)
    robot2 = Robot.of(player1Id, planet2)
    robot3 = Robot.of(player1Id, planet1)
    robot4 = Robot.of(player2Id, planet1)
    robot5 = Robot.of(player2Id, planet2)
    robot6 = Robot.of(player2Id, planet1)

    robotRepository.saveAll(arrayListOf(robot1, robot2, robot3, robot4, robot5, robot6))
  }

  @Test
  fun `Saved Robots can be retrieved`() {
    // when
    robotRepository.save(robot1)

    // then
    assertEquals(robot1.id, robotRepository.findByIdOrNull(robot1.id)!!.id)
  }

  @Test
  fun `Retrieves the dead robots on a planet`() {
    // given
    robot1.alive = false
    robot3.alive = false

    robotRepository.save(robot1)
    robotRepository.save(robot3)

    // when
    val robotIds =
      robotRepository.findDeadRobotsOnPlanet(planet1.planetId).map { it.id }

    // then
    assertAll(
      {
        assert(robotIds.contains(robot1.id))
      },
      {
        assert(robotIds.contains(robot3.id))
      }
    )
  }

  @Test
  fun `FindAllByPlanet_PlanetId returns all robots on the planet`() {
    val robotIds = robotRepository.findAllRobotsOnPlanet(planet1.planetId).map { it.id }
    assertAll(
      {
        assert(robotIds.contains(robot1.id))
      },
      {
        assert(robotIds.contains(robot3.id))
      },
      {
        assert(robotIds.contains(robot4.id))
      },
      {
        assert(robotIds.contains(robot6.id))
      }
    )
  }
}
