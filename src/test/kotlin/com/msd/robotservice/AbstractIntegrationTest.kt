package com.msd.robotservice

import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.redpanda.RedpandaContainer

@SpringBootTest
@ActiveProfiles(profiles = ["test"])
abstract class AbstractIntegrationTest {
  companion object {
    private val redpanda = RedpandaContainer("docker.redpanda.com/vectorized/redpanda:v22.3.5")

    init {
      redpanda.start()
    }

    @JvmStatic
    @DynamicPropertySource
    fun kafkaProperties(registry: DynamicPropertyRegistry) {
      registry.add("spring.kafka.bootstrap-servers", redpanda::getBootstrapServers)
    }
  }
}
