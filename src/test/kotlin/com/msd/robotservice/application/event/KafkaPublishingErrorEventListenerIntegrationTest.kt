package com.msd.robotservice.application.event

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.msd.robotservice.AbstractIntegrationTest
import org.apache.kafka.clients.producer.ProducerRecord
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.mockito.kotlin.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.context.ApplicationEventPublisher
import org.springframework.kafka.core.KafkaTemplate
import java.util.*

internal class KafkaPublishingErrorEventListenerIntegrationTest : AbstractIntegrationTest() {
  @Autowired
  private lateinit var objectMapper: ObjectMapper

  @MockBean
  private lateinit var template: KafkaTemplate<String, String>

  @Autowired
  private lateinit var applicationEventPublisher: ApplicationEventPublisher

  data class TestErrorEvent(override val playerId: UUID, override val transactionId: UUID) :
    ErrorEvent(playerId, transactionId, UUID.randomUUID(), "TEST_ERROR", "Test error")

  @Test
  fun `should publish error event from eventBus to Kafka`() {
    val playerId = UUID.randomUUID()
    val event = TestErrorEvent(playerId, UUID.randomUUID())
    whenever(template.send(any<ProducerRecord<String, String>>())).thenReturn(mock())

    // When
    applicationEventPublisher.publishEvent(event)

    // Then
    val captor = argumentCaptor<ProducerRecord<String, String>>()
    verify(template).send(captor.capture())
    assertThat(captor.allValues)
      .hasSize(1)

    val firstValue = captor.firstValue
    assertThat(firstValue.key())
      .isEqualTo(event.playerId.toString())
    assertThat(firstValue.value())
      .matches { objectMapper.readValue<TestErrorEvent>(it) == event }
    assertThat(firstValue.headers())
      .anyMatch { it.key() == "type" && it.value().isNotEmpty() }
      .anyMatch { it.key() == "timestamp" && it.value().isNotEmpty() }
      .anyMatch {
        it.key() == "playerId" && it.value().contentEquals(playerId.toString().encodeToByteArray())
      }
  }
}
