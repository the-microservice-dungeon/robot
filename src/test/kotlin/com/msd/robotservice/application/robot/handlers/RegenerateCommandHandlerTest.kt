package com.msd.robotservice.application.robot.handlers

import com.msd.robotservice.application.command.RobotCommand
import com.msd.robotservice.application.robot.event.RobotEventPublisher
import com.msd.robotservice.application.robot.event.RobotRegeneratedEvent
import com.msd.robotservice.domain.planet.Planet
import com.msd.robotservice.domain.robot.Robot
import com.msd.robotservice.domain.robot.RobotRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.mockito.kotlin.*
import java.util.*

internal class RegenerateCommandHandlerTest {
  private val robotRepository = mock<RobotRepository>()
  private val eventPublisher = mock<RobotEventPublisher>()
  private val regenerateCommandHandler = RegenerateCommandHandler(robotRepository, eventPublisher)

  @Test
  fun shouldRegenerate() {
    val planet = Planet.of(UUID.randomUUID(), UUID.randomUUID())
    val robot = Robot.of(UUID.randomUUID(), planet)
    whenever(robotRepository.findByIdOrThrow(robot.id)).thenReturn(robot)

    robot.reduceEnergy(10)
    val energyBefore = robot.energy

    val command = RobotCommand(
      robot.id,
      UUID.randomUUID(),
      robot.player,
      RobotCommand.CommandType.REGENERATE,
      null,
      null
    )

    regenerateCommandHandler.handle(command)
    assertThat(robot.energy)
      .isGreaterThan(energyBefore)
    verify(robotRepository).save(robot)
    verify(eventPublisher).publish(any<RobotRegeneratedEvent>(), eq(command.transactionId))
  }
}
