package com.msd.robotservice.application.robot.handlers

import com.msd.robotservice.application.command.RobotCommand
import com.msd.robotservice.application.robot.event.RobotAttackedEvent
import com.msd.robotservice.application.robot.event.RobotEventPublisher
import com.msd.robotservice.domain.planet.Planet
import com.msd.robotservice.domain.robot.Robot
import com.msd.robotservice.domain.robot.RobotRepository
import com.msd.robotservice.domain.robot.exception.TargetRobotOutOfReachException
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Test
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import org.mockito.kotlin.any
import org.mockito.kotlin.eq
import org.mockito.kotlin.mock
import java.util.*

internal class FightingCommandHandlerTest {
  private val robot = Robot.of(UUID.randomUUID(), Planet.of(UUID.randomUUID(), UUID.randomUUID()))
  private val robotRepository = mock<RobotRepository>()
  private val eventPublisher = mock<RobotEventPublisher>()
  private val fightingCommandHandler = FightingCommandHandler(robotRepository, eventPublisher)

  @Test
  fun shouldExecuteAttack() {
    val targetRobot = Robot.of(UUID.randomUUID(), robot.planet)
    val healthStart = targetRobot.health

    `when`(robotRepository.findByIdOrThrow(robot.id)).thenReturn(robot)
    `when`(robotRepository.findByIdOrThrow(targetRobot.id)).thenReturn(targetRobot)

    val command = RobotCommand(
      robot.id,
      UUID.randomUUID(),
      robot.player,
      RobotCommand.CommandType.BATTLE,
      null,
      targetRobot.id,
    )

    fightingCommandHandler.handle(command)

    assertThat(targetRobot.health)
      .isLessThan(healthStart)
    verify(robotRepository).save(robot)
    verify(robotRepository).save(targetRobot)
    verify(eventPublisher).publish(any<RobotAttackedEvent>(), eq(command.transactionId))
  }

  @Test
  fun shouldThrowWhenNotOnTheSamePlanet() {
    val targetRobot = Robot.of(UUID.randomUUID(), Planet.of(UUID.randomUUID(), UUID.randomUUID()))

    `when`(robotRepository.findByIdOrThrow(robot.id)).thenReturn(robot)
    `when`(robotRepository.findByIdOrThrow(targetRobot.id)).thenReturn(targetRobot)

    val command = RobotCommand(
      robot.id,
      UUID.randomUUID(),
      robot.player,
      RobotCommand.CommandType.BATTLE,
      null,
      targetRobot.id
    )

    assertThatThrownBy {  fightingCommandHandler.handle(command) }
      .isInstanceOf(TargetRobotOutOfReachException::class.java)
  }
}
