package com.msd.robotservice.application.robot.handlers

import com.msd.robotservice.application.robot.event.RobotEventPublisher
import com.msd.robotservice.application.robot.event.RobotSpawnedEvent
import com.msd.robotservice.domain.planet.Planet
import com.msd.robotservice.domain.planet.PlanetRepository
import com.msd.robotservice.domain.robot.Robot
import com.msd.robotservice.domain.robot.RobotRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.mockito.kotlin.*
import java.util.*

internal class SpawnRobotHandlerTest {
  private val robotRepository = mock<RobotRepository>()
  private val planetRepository = mock<PlanetRepository>()
  private val eventPublisher = mock<RobotEventPublisher>()
  private val spawnRobotHandler = SpawnRobotHandler(planetRepository, robotRepository, eventPublisher)

  @Test
  fun `should spawn Robot`() {
    // Given
    val player = UUID.randomUUID()
    val planetId = UUID.randomUUID()
    val planet = Planet.of(planetId, UUID.randomUUID())
    val transactionId = UUID.randomUUID()

    whenever(planetRepository.getRandomPlanet()).thenReturn(planet)

    // When
    spawnRobotHandler.handle(player, transactionId)
    val captor = argumentCaptor<Robot>()
    verify(robotRepository).save(captor.capture())

    // Then
    assertThat(captor.firstValue)
      .isNotNull
      .matches { it.player == player }
      .matches { it.planet.planetId == planetId }
    verify(eventPublisher).publish(any<RobotSpawnedEvent>(), eq(transactionId))
  }
}
