package com.msd.robotservice.application.robot.handlers

import com.msd.robotservice.application.command.RobotCommand
import org.junit.jupiter.api.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.springframework.context.ApplicationEventPublisher
import java.util.UUID

internal class MediatingCommandDispatcherTest {
  private val fightingCommandHandler = mock(FightingCommandHandler::class.java)
  private val regeneratingCommandHandler = mock(RegenerateCommandHandler::class.java)
  private val miningCommandHandler = mock(MiningCommandHandler::class.java)
  private val movementCommandHandler = mock(MovementCommandHandler::class.java)
  private val applicationEventPublisher = mock(ApplicationEventPublisher::class.java)
  private val mediatingCommandDispatcher = MediatingCommandDispatcher(fightingCommandHandler, miningCommandHandler, movementCommandHandler, regeneratingCommandHandler, applicationEventPublisher)

  // We just test that the correct handler is called for each command type so we can be sure that the MediatingCommandHandler delegates to the correct handler
  @Test
  fun `should call fighting handler`() {
    val command = buildCommand(RobotCommand.CommandType.BATTLE)
    mediatingCommandDispatcher.handle(command)
    verify(fightingCommandHandler).handle(command)
  }

  @Test
  fun `should call mining handler`() {
    val command = buildCommand(RobotCommand.CommandType.MINING)
    mediatingCommandDispatcher.handle(command)
    verify(miningCommandHandler).handle(command)
  }

  @Test
  fun `should call movement handlers`() {
    val command = buildCommand(RobotCommand.CommandType.MOVEMENT)
    mediatingCommandDispatcher.handle(command)
    verify(movementCommandHandler).handle(command)
  }

  @Test
  fun `should call regenerate handler`() {
    val command = buildCommand(RobotCommand.CommandType.REGENERATE)
    mediatingCommandDispatcher.handle(command)
    verify(regeneratingCommandHandler).handle(command)
  }

  private fun buildCommand(type: RobotCommand.CommandType): RobotCommand {
    return RobotCommand(
      UUID.randomUUID(),
      UUID.randomUUID(),
      UUID.randomUUID(),
      type,
      UUID.randomUUID(),
      UUID.randomUUID()
    )
  }
}
