package com.msd.robotservice.infrastructure.map

import com.msd.robotservice.AbstractIntegrationTest
import com.msd.robotservice.domain.planet.Planet
import com.msd.robotservice.domain.planet.PlanetRepository
import com.msd.robotservice.domain.planet.ResourceType
import com.msd.robotservice.domain.robot.Robot
import com.msd.robotservice.domain.robot.RobotRepository
import org.apache.kafka.clients.producer.ProducerRecord
import org.assertj.core.api.Assertions
import org.assertj.core.api.SoftAssertions
import org.awaitility.Awaitility.await
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.core.KafkaTemplate
import java.util.*
import java.util.concurrent.TimeUnit

// It can happen that the template sends records to the broker before the consumer has subscribed
// to the topic. To overcome this, we simply adjust the offset-reset to earliest (default: latest)
internal class MapEventListenerIntegrationTest : AbstractIntegrationTest() {
  @Autowired
  lateinit var kafkaTemplate: KafkaTemplate<String, String>

  @Autowired
  lateinit var planetRepository: PlanetRepository

  @Autowired
  lateinit var robotRepository: RobotRepository

  private fun resourceJson(
    resourceType: String = "coal",
    maxAmount: Int = 0,
    currentAmount: Int = 0
  ): String {
    return """
      {
        "resourceType": "$resourceType",
        "maxAmount": $maxAmount,
        "currentAmount": $currentAmount
      }
    """.trimIndent()
  }

  private fun planetJson(
    id: String = UUID.randomUUID().toString(),
    x: Int = 0,
    y: Int = 0,
    movementDifficulty: Int = 1,
    resource: String? = null
  ): String {
    return """
      {
        "id": "$id",
        "x": $x,
        "y": $y,
        "movementDifficulty": $movementDifficulty,
        "resource": ${resource ?: "null"}
      }
    """.trimIndent()
  }

  @Test
  fun `should save single planet on gameworld-created`() {
    val gameworldId = UUID.randomUUID()
    val message = """
      {
        "id": "$gameworldId",
        "status": "ACTIVE",
        "planets": [
          ${
    planetJson(
      id = "497f6eca-6276-4993-bfeb-53cbbbba6f08",
      x = 0,
      y = 0,
      movementDifficulty = 2,
      resource = resourceJson(resourceType = "COAL")
    )
    }
        ]
      }
    """.trimIndent()
    val record =
      ProducerRecord("gameworld", gameworldId.toString(), message)
    record.headers().add("type", "GameworldCreated".toByteArray())
    kafkaTemplate.send(record).addCallback({}, { throw it })

    val planetId = UUID.fromString("497f6eca-6276-4993-bfeb-53cbbbba6f08")
    val planet = await().atMost(10, TimeUnit.SECONDS)
      .until(
        { planetRepository.findById(planetId) },
        { it.isPresent }
      ).get()

    Assertions.assertThat(planet.planetId).isEqualTo(planetId)
    Assertions.assertThat(planet.gameWorldId).isEqualTo(gameworldId)
    Assertions.assertThat(planet.movementDifficulty).isEqualTo(2)
    Assertions.assertThat(planet.resourceType).isEqualTo(ResourceType.COAL)
  }

  @Test
  fun `should not save its own as neighbour`() {
    val gameworldId = UUID.randomUUID()
    val message = """
      {
        "id": "$gameworldId",
        "status": "ACTIVE",
        "planets": [
          ${
    planetJson(
      id = "497f6eca-6276-4993-bfeb-53cbbbba6f08",
      x = 0,
      y = 0,
      movementDifficulty = 1,
      resource = resourceJson(resourceType = "COAL")
    )
    }
        ]
      }
    """.trimIndent()
    val record =
      ProducerRecord("gameworld", gameworldId.toString(), message)
    record.headers().add("type", "GameworldCreated".toByteArray())
    kafkaTemplate.send(record)

    val planetId = UUID.fromString("497f6eca-6276-4993-bfeb-53cbbbba6f08")
    val planet = await().atMost(10, TimeUnit.SECONDS)
      .until(
        { planetRepository.findById(planetId) },
        { it.isPresent }
      ).get()

    Assertions.assertThat(planet.neighbours).filteredOn { it.planetId == planetId }.isEmpty()
  }

  @Test
  fun `should save neighbours`() {
    val gameworldId = UUID.randomUUID()

    val message = """
      {
        "id": "$gameworldId",
        "status": "active",
        "planets": [
          ${planetJson(id = "00000000-0000-0000-0000-000000000000", x = 0, y = 0)},
          ${planetJson(id = "00000000-0000-0000-0000-000000000001", x = 1, y = 0)},
          ${planetJson(id = "00000000-0000-0000-0000-000000000002", x = 2, y = 0)},
          ${planetJson(id = "00000000-0000-0000-0000-000000000003", x = 0, y = 1)},
          ${planetJson(id = "00000000-0000-0000-0000-000000000004", x = 1, y = 1)},
          ${planetJson(id = "00000000-0000-0000-0000-000000000005", x = 2, y = 1)},
          ${planetJson(id = "00000000-0000-0000-0000-000000000006", x = 0, y = 2)},
          ${planetJson(id = "00000000-0000-0000-0000-000000000007", x = 1, y = 2)},
          ${planetJson(id = "00000000-0000-0000-0000-000000000008", x = 2, y = 2)}
        ]
      }
    """.trimIndent()
    val record =
      ProducerRecord("gameworld", gameworldId.toString(), message)
    record.headers().add("type", "GameworldCreated".toByteArray())
    kafkaTemplate.send(record).addCallback({}, { throw it })

    await().atMost(10, TimeUnit.SECONDS)
      .until(
        { planetRepository.findPlanetsInGameworld(gameworldId) },
        { planets -> planets.size == 9 && planets.all { it.neighbours.size > 0 } }
      )

    val expectedNeighbors = mapOf(
      Pair("00000000-0000-0000-0000-000000000000", listOf("00000000-0000-0000-0000-000000000001", "00000000-0000-0000-0000-000000000003")),
      Pair("00000000-0000-0000-0000-000000000001", listOf("00000000-0000-0000-0000-000000000002", "00000000-0000-0000-0000-000000000000", "00000000-0000-0000-0000-000000000004")),
      Pair("00000000-0000-0000-0000-000000000002", listOf("00000000-0000-0000-0000-000000000001", "00000000-0000-0000-0000-000000000005")),
      Pair("00000000-0000-0000-0000-000000000003", listOf("00000000-0000-0000-0000-000000000000", "00000000-0000-0000-0000-000000000004", "00000000-0000-0000-0000-000000000006")),
      Pair("00000000-0000-0000-0000-000000000004", listOf("00000000-0000-0000-0000-000000000001", "00000000-0000-0000-0000-000000000003", "00000000-0000-0000-0000-000000000005", "00000000-0000-0000-0000-000000000007")),
      Pair("00000000-0000-0000-0000-000000000005", listOf("00000000-0000-0000-0000-000000000002", "00000000-0000-0000-0000-000000000004", "00000000-0000-0000-0000-000000000008")),
      Pair("00000000-0000-0000-0000-000000000006", listOf("00000000-0000-0000-0000-000000000003", "00000000-0000-0000-0000-000000000007")),
      Pair("00000000-0000-0000-0000-000000000007", listOf("00000000-0000-0000-0000-000000000006", "00000000-0000-0000-0000-000000000004", "00000000-0000-0000-0000-000000000008")),
      Pair("00000000-0000-0000-0000-000000000008", listOf("00000000-0000-0000-0000-000000000007", "00000000-0000-0000-0000-000000000005"))
    )

    val softAssert = SoftAssertions()
    expectedNeighbors.forEach { (planetId, expectedNeighbourIds) ->
      run {
        val planet = planetRepository.findById(UUID.fromString(planetId)).get()
        softAssert.assertThat(planet.neighbours.map { it.planetId.toString() })
          .containsExactlyInAnyOrderElementsOf(expectedNeighbourIds)
      }
    }

    softAssert.assertAll()
  }
}
