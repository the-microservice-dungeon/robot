package com.msd.robotservice.infrastructure.trading

import com.msd.robotservice.AbstractIntegrationTest
import com.msd.robotservice.domain.planet.Planet
import com.msd.robotservice.domain.planet.ResourceType
import com.msd.robotservice.domain.robot.Robot
import com.msd.robotservice.domain.robot.RobotRepository
import com.msd.robotservice.domain.robot.UpgradeType
import org.awaitility.Awaitility.await
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.EnumSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.core.KafkaTemplate
import java.util.*
import java.util.concurrent.TimeUnit

internal class TradingEventListenerIntegrationTest : AbstractIntegrationTest() {
  @Autowired
  lateinit var kafkaTemplate: KafkaTemplate<String, String>

  @Autowired
  lateinit var robotRepository: RobotRepository

  @Test
  fun `should remove resources when sold`() {
    // Given
    val playerId = UUID.randomUUID()
    val robot = Robot.of(playerId, Planet.of(UUID.randomUUID(), UUID.randomUUID()))
    robot.addResource(ResourceType.GEM, 10)
    robotRepository.save(robot)

    // When
    kafkaTemplate.send(
      "trade-sell",
      """
      {
        "playerId":"$playerId",
        "robotId":"${robot.id}",
        "type":"RESOURCE",
        "name":"GEM",
        "amount":1,
        "pricePerUnit":30,
        "totalPrice":30
      }
      """
    )

    // Then
    await().atMost(10, TimeUnit.SECONDS).until {
      robotRepository.findById(robot.id).get().inventory.getResourceAmount(ResourceType.GEM) == 9
    }
  }

  @Test
  fun `should restore energy when bought`() {
    // Given
    val playerId = UUID.randomUUID()
    val robot = Robot.of(playerId, Planet.of(UUID.randomUUID(), UUID.randomUUID()))
    robot.reduceEnergy(2)
    robotRepository.save(robot)

    // When
    kafkaTemplate.send(
      "trade-buy",
      """
      {
        "playerId":"$playerId",
        "robotId": "${robot.id}",
        "type":"RESTORATION",
        "name":"ENERGY_RESTORE",
        "amount":1,
        "pricePerUnit":100,
        "totalPrice":200
      }
      """
    )

    // Then
    await().atMost(10, TimeUnit.SECONDS).until {
      robotRepository.findById(robot.id).get().energy == robot.maxEnergy
    }
  }

  @Test
  fun `should restore health when bought`() {
    // Given
    val playerId = UUID.randomUUID()
    val robot = Robot.of(playerId, Planet.of(UUID.randomUUID(), UUID.randomUUID()))
    robot.receiveDamage(2)
    robotRepository.save(robot)

    // When
    kafkaTemplate.send(
      "trade-buy",
      """
      {
        "playerId":"$playerId",
        "robotId": "${robot.id}",
        "type":"RESTORATION",
        "name":"HEALTH_RESTORE",
        "amount":1,
        "pricePerUnit":100,
        "totalPrice":200
      }
      """
    )

    // Then
    await().atMost(10, TimeUnit.SECONDS).until {
      robotRepository.findById(robot.id).get().health == robot.maxHealth
    }
  }

  @ParameterizedTest
  @EnumSource(UpgradeType::class)
  fun `should upgrade robot when bought`(upgradeType: UpgradeType) {
    // Given
    val playerId = UUID.randomUUID()
    val robot = Robot.of(playerId, Planet.of(UUID.randomUUID(), UUID.randomUUID()))
    robotRepository.save(robot)

    // When
    kafkaTemplate.send(
      "trade-buy",
      """
      {
        "playerId":"$playerId",
        "robotId":"${robot.id}",
        "type":"UPGRADE",
        "name":"${upgradeType.name}_1",
        "amount":1,
        "pricePerUnit":100,
        "totalPrice":200
      }
      """
    )

    // Then
    await().atMost(10, TimeUnit.SECONDS).until {
      var robot = robotRepository.findById(robot.id).get()
      when (upgradeType) {
        UpgradeType.DAMAGE -> robot.damageLevel == 1
        UpgradeType.HEALTH -> robot.healthLevel == 1
        UpgradeType.ENERGY_REGEN -> robot.energyRegenLevel == 1
        UpgradeType.MAX_ENERGY -> robot.energyLevel == 1
        UpgradeType.MINING_SPEED -> robot.miningSpeedLevel == 1
        UpgradeType.MINING -> robot.miningLevel == 1
        UpgradeType.STORAGE -> robot.inventory.storageLevel == 1
        else -> false
      }
    }
  }

  @Test
  fun `should spawn robot when robot has been bought`() {
    // Given
    val playerId = UUID.randomUUID()

    // When
    kafkaTemplate.send(
      "trade-buy",
      """
      {
        "playerId":"$playerId",
        "robotId":null,
        "type":"ITEM",
        "name":"ROBOT",
        "amount":2,
        "pricePerUnit":100,
        "totalPrice":200
      }
      """
    )

    // Then
    await().atMost(10, TimeUnit.SECONDS).until {
      robotRepository.findAllByPlayer(playerId).size == 2
    }
  }
}
